Document: debian-history.ru
Title: A Short History of the Debian Project, Russian translation
Author: Bdale Garbee, Petr Novodvorskij, Pavel Romanchenko and others
Abstract: A Short History of the Debian Project
Section: Debian

Format: HTML
Index: /usr/share/doc/debian-history/docs/index.ru.html
Files: /usr/share/doc/debian-history/docs/*.ru.html

Format: pdf
Files: /usr/share/doc/debian-history/docs/*.ru.pdf

Format: text
Files: /usr/share/doc/debian-history/docs/*.ru.txt.gz
