Source: debian-history
Section: doc
Priority: optional
Maintainer: Bdale Garbee <bdale@gag.com>
Uploaders: Osamu Aoki <osamu@debian.org>, Javier Fernandez-Sanguino <jfs@debian.org>
Build-Depends: debhelper-compat (= 12)
Build-Depends-Indep: dblatex,
                     docbook-xml,
                     docbook-xsl,
                     fonts-liberation2,
                     fonts-vlgothic,
                     fonts-wqy-microhei,
                     libxml2-utils,
                     locales-all | locales,
                     po4a,
                     texlive-lang-all,
                     latex-cjk-all,
                     texlive-xetex,
                     w3m,
                     xsltproc,
                     zip
Standards-Version: 4.5.0
Homepage: https://www.debian.org/doc/misc-manuals#history
Vcs-Git: https://salsa.debian.org/ddp-team/project-history.git
Vcs-Browser: https://salsa.debian.org/ddp-team/project-history

Package: debian-history
Architecture: all
Depends: ${misc:Depends}
Multi-Arch: foreign
Description: Short History of the Debian Project
 As the Debian community continues to grow, and "old timers" become fewer
 and farther between, it seems appropriate to document where the project
 came from, and what it is about.
 .
 The package includes the original document (in English) as well as the
 translations in French, German, Italian, Japanese, Korean, Portuguese,
 Russian, Spanish, and Lithuanian.
